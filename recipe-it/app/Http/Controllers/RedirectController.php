<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Class RedirectController, this controller allows to perform redirect method.
 * @package App\Http\Controllers
 * @author arivadeneria
 */
class RedirectController extends Controller
{
    /**
     * this method allows to redirect to another page in to the project, using identifier id.
     * @param $pageName
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectPage($pageName){
        return redirect()->route($pageName);
    }
}
