<?php

namespace App\Http\Controllers\Recipe;

use App\Http\Controllers\Controller;
use App\Models\Phonebook;
use Illuminate\Http\Request;

class PhonebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phoneBook = Phonebook::paginate(2);
        return view('phonebook.index', compact('phoneBook'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('phonebook.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $phoneBookInstance = new Phonebook;
        $phoneBookInstance->name = $request->name;
        $phoneBookInstance->lastName = $request->lastName;
        $phoneBookInstance->telephone = $request->telephone;
        $phoneBookInstance->gender = $request->gender;
        $phoneBookInstance->role = $request->role;
        $phoneBookInstance->email = $request->email;
        $phoneBookInstance->salary = $request->salary;
        $phoneBookInstance->save();
        return redirect()->route('phoneBook.index')->with('data',"The register implementation is success");
        //return 'The register implementation is success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phoneBook = Phonebook::findOrFail($id);
        $phoneBook->delete();
        return redirect()->route('phoneBook.index')->with('data',"The register was deleting successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function confirm($id)
    {
        $phoneBook = Phonebook::findOrFail($id);
        return view('phonebook.confirm', compact('phoneBook'));
    }
}
