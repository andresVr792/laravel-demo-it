<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/**
 * test route.
 */
Route::get('/test', function () {
    return 'Hi! It is a test page';
})->name('testRoute');

/**
 * re-direct route with page parameter, this route allows to change pages in project.
 */
Route::get('/redirect/{pageName}', 'RedirectController@redirectPage')->where('pageName','[A-Za-z]+');

Route::resource('phoneBook','Recipe\PhonebookController');

Route::get('/cancel', function() {
    return redirect()->route('phoneBook.index')->with('cancel','Cancel Action!');

})->name('cancel');

Route::get('/phonebook/{id}/confirm','Recipe\PhonebookController@confirm' )->name('phoneBook.confirm');
