<br>

<footer>

    <section class="footers border  pt-5 pb-3">
        <div class="{{ $container }} pt-5">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 footers-one ">
                    <div class="footers-logo">
                        <img class="img-responsive" src="https://imge.apk.tools/300/d/3/1/com.widesoft.guiatelefonica.png" style="width: 50px;
">

                    </div>
                    <div class="footers-info mt-3">
                        <p>Cras sociis natoque penatibus et magnis Lorem Ipsum tells about the compmany right now the best.</p>
                    </div>
                    <div class="social-icons">
                        <a href="https://www.facebook.com/"><i id="social-fb" class="fab fa-facebook-square fa-2x social"></i></a>
                        <a href="https://twitter.com/"><i id="social-tw" class="fab fa-twitter-square fa-2x social"></i></a>
                        <a href="https://plus.google.com/"><i id="social-gp" class="fab fa-google-plus-square fa-2x social"></i></a>
                        <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-2x social"></i></a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 footers-two">
                    <h5>Essentials </h5>
                    <ul class="list-unstyled">
                        <li><a href="maintenance.html">Search</a></li>
                        <li><a href="contact.html">Sell your Car</a></li>


                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 footers-three">
                    <h5>Information </h5>
                    <ul class="list-unstyled">
                        <li><a href="maintenance.html">Register Now</a></li>
                        <li><a href="contact.html">Advice</a></li>


                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 footers-four">
                    <h5>Explore </h5>
                    <ul class="list-unstyled">
                        <li><a href="maintenance.html">News</a></li>
                        <li><a href="contact.html">Sitemap</a></li>


                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 footers-five">
                    <h5>Company </h5>
                    <ul class="list-unstyled">
                        <li><a href="maintenance.html">Career</a></li>
                        <li><a href="about.html">For Parters</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <section class="copyright border">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12 pt-3">
                    <p class="text-muted">© 2019 Vr Ecuador & Italy.</p>
                </div>
            </div>
        </div>
    </section>




</footer>
