@extends('template.baseTemplate')
@section('title','Phone Book')
@section('content')
    <div class="container-fluid registerinicio">
        <div class="row">
            <div class="col-md-6 register-left register-left1">
                <img src="http://www.idaipqroo.org.mx/wp-content/uploads/2018/06/proteccion-de-datos-personales-791x1024.png" alt=""/>
            </div>
            <div class="col-md-6 register-left">

                <h3>Welcome</h3>
                <p>Please complete all of the fields correctly!</p>

            </div>
        </div>
    </div>



    <div class="container-fluid ">
        @if (session('data'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{session('data')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
            @if ( session('cancel')  )
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session('cancel') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

        <br>
        <nav class="navbar navbar-light">
            <a class="navbar-brand"><img id="icono" class="img-responsive"
                                         src="https://imge.apk.tools/300/d/3/1/com.widesoft.guiatelefonica.png"></a>

            <ul class="nav flex-column text-center">
                <li class="nav-item">

                </li>
                <li class="nav-item">

                </li>
            </ul>

        </nav>


        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Library</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>

        <br>
        <h1 class="text-center">Phone Book</h1>

        <br>
        <div class="row float-right">
            <a href="{{route('phoneBook.create')}}" class="btn btn-info btncolorblanco"><i class="fas fa-user-plus"></i> Create New Register</a>
        </div>
        <br>
            <div class="col-md-12">
        <table class="table-responsive table text-center col-md-12">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Telephone</th>
                <th scope="col">Gender</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                <th scope="col">Salary</th>
                <th scope="col">Action</th>

            </tr>
            </thead>
            <tbody>
            @foreach($phoneBook as $phoneBookItem)


            <tr>
                <th scope="row">{{ $phoneBookItem->id }}</th>
                <td>{{$phoneBookItem->name}} {{$phoneBookItem->lastName}}</td>
                <td>{{$phoneBookItem->telephone}}</td>
                <td>{{$phoneBookItem->gender}}</td>
                <td>{{$phoneBookItem->email}}</td>
                <td>{{$phoneBookItem->salary}}</td>
                <td>{{$phoneBookItem->role}}</td>
                <td><a class="btn btn-success btncolorblanco">
                        <i class="fa fa-edit"></i> Edit
                    </a>

                    <a href="{{ route('phoneBook.confirm', $phoneBookItem->id) }}" class="btn btn-danger btncolorblanco">
                        <i class="fa fa-trash-alt"></i> Delete
                    </a>
                </td>

            </tr>
            @endforeach
            </tbody>
        </table>
                {{$phoneBook}}
            </div>

    </div>

@include('template.footer', ['container' => 'container-fluid'])
@endsection
