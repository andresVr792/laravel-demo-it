@extends('template.baseTemplate')

@section('title','Confirm popup')


@section('content')
    <div class="container py-5">
        <h1>¿Do you want to delete the register {{ $phoneBook->name }} {{ $phoneBook->lastname }}? </h1>

        <form method="post" action="{{ route('phoneBook.destroy', $phoneBook->id )}}">
            @method('DELETE')
            @csrf
            <button type="submit" class="redondo btn btn-danger">
                <i class="fas fa-trash-alt"></i> Delete
            </button>
            <a class="redondo btn btn-secondary" href="{{ route('cancel') }}">
                <i class="fas fa-ban"></i> Cancel
            </a>
        </form>

    </div>


    @include('template.footer',['container'=>'container'])

@endsection
