@extends('template.baseTemplate')

@section('title','Create')

@section('content')



    <div class="container">
        <br>
        <nav class="navbar navbar-light">
            <a class="navbar-brand"><img id="icono" class="img-responsive"
                                         src="https://imge.apk.tools/300/d/3/1/com.widesoft.guiatelefonica.png"></a>

            <ul class="nav flex-column text-center">
                <li class="nav-item">
                    <span class="nav-link active">Welcome</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">log out</a>
                </li>
            </ul>

        </nav>


        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Library</a></li>
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>
    </div>

    <form method="POST" action="{{ route('phoneBook.store') }}">
        @csrf
        <div class="container register">


            <div class="row">
                <div class="col-md-3 register-left">
                    <img src="http://www.idaipqroo.org.mx/wp-content/uploads/2018/06/proteccion-de-datos-personales-791x1024.png" alt=""/>
                    <h3>Bienvenid@</h3>
                    <p>Por favor llena los datos correctamente en el sistema!</p>

                </div>
                <div class="col-md-9 register-right">

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                            <h3 class="register-heading">Crear nuevo Registro</h3>

                            <div class="row register-form">

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-user text-info"></i></div>
                                            </div>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-user-edit text-info"></i></div>
                                            </div>
                                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="lastName" required="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-phone text-info"></i></div>
                                            </div>
                                            <input class="form-control" type="number" name="telephone" placeholder="telephone: +593 996057105" id="Telephone">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="maxl">
                                            <label class="radio inline">
                                                <input type="radio" name="gender" value="Male" checked>
                                                <span> Male </span>
                                            </label>
                                            <label class="radio inline">
                                                <input type="radio" name="gender" value="Female">
                                                <span>Female </span>
                                            </label>
                                            <label class="radio inline">
                                                <input type="radio" name="gender" value="GLBTI">
                                                <span>GLBTI </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">


                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa  fa-at text-info"></i></div>
                                            </div>
                                            <input type="email" name="email" class="form-control" placeholder="Email" value="" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                                            </div>
                                            <select name="role" class="form-control">
                                                <option class="hidden" selected disabled>Role</option>
                                                <option>Chef</option>
                                                <option>Junior chef</option>
                                                <option>Master chef</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fa  fa-dollar-sign text-info"></i></div>
                                            </div>
                                            <input type="number" class="form-control" name="salary" placeholder="salary *" value="" />
                                        </div>
                                    </div>





                                    <button type="submit" class="redondo btn btn-info"><i class="fas fa-save"></i> Guardar</button>
                                    <a href="{{ route('cancel') }}" class="redondo btn btn-danger"><i class="fas fa-ban"></i> Cancelar</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </form>


    @include('template.footer',['container'=>'container'])


@endsection
